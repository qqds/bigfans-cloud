package com.bigfans.catalogservice.model;

import com.bigfans.catalogservice.model.entity.StockEntity;
import lombok.Data;

/**
 * @author lichong
 * @create 2018-03-20 下午7:54
 **/
@Data
public class Stock extends StockEntity {
}
