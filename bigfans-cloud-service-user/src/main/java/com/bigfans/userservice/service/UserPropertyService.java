package com.bigfans.userservice.service;

import java.math.BigDecimal;

public interface UserPropertyService {

    void useProperty(String userId, String orderId, String couponId, Float points, BigDecimal balance) throws Exception;

    void useCoupon(String userId, String orderId, String couponId) throws Exception;

    void usePoints(String userId, String orderId, Float points) throws Exception;

    void useBalance(String userId, String orderId, BigDecimal balance) throws Exception;

    void revertOrderProperty(String orderId) throws Exception;

    void revertOrderUsedCoupon(String orderId) throws Exception;

    void revertOrderUsedPoints(String orderId) throws Exception;

    void revertOrderUsedBalance(String orderId) throws Exception;

    void addCoupon(String userId , String couponId, Integer count) throws Exception;
}
