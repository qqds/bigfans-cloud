package com.bigfans.searchservice.model;

import com.bigfans.framework.model.AbstractModel;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 
 * @Title:
 * @Description: 商品标签,用于商品搜索提示
 * @author lichong
 * @date 2016年2月29日 上午9:36:00
 * @version V1.0
 */
@Table(name = "Tag")
public class Tag extends AbstractModel {

	private static final long serialVersionUID = 8640764354408862116L;

	@Column(name = "name")
	protected String name;
	// 关联到该标签的商品数量
	@Column(name="related_count")
	private Integer relatedCount;

	public Integer getRelatedCount() {
		return relatedCount;
	}

	public void setRelatedCount(Integer relatedCount) {
		this.relatedCount = relatedCount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getModule() {
		return "Tag";
	}

}
