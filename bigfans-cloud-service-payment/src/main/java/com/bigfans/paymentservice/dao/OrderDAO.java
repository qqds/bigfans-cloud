package com.bigfans.paymentservice.dao;


import com.bigfans.framework.dao.BaseDAO;
import com.bigfans.paymentservice.model.Order;

/**
 * 
 * @Description:订单Mapper
 * @author lichong 
 * 2014年12月14日下午4:05:14
 *
 */
public interface OrderDAO extends BaseDAO<Order> {

}
