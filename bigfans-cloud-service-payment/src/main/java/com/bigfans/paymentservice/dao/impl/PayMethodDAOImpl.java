package com.bigfans.paymentservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import com.bigfans.paymentservice.dao.PayMethodDAO;
import com.bigfans.paymentservice.model.PayMethod;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 
 * @Description:
 * @author lichong
 * 2015年4月4日下午9:27:43
 *
 */
@Repository(PayMethodDAOImpl.BEAN_NAME)
public class PayMethodDAOImpl extends MybatisDAOImpl<PayMethod> implements PayMethodDAO {

	public static final String BEAN_NAME = "payMethodDAO";

	@Override
	public PayMethod getByCode(String code) {
		ParameterMap params = new ParameterMap();
		params.put("code", code);
		return getSqlSession().selectOne(className + ".load", params);
	}

	@Override
	public List<PayMethod> listTopMethods() {
		return getSqlSession().selectList(className + ".listTopMethods");
	}

	@Override
	public List<PayMethod> listByType(String type) {
		return null;
	}
}
