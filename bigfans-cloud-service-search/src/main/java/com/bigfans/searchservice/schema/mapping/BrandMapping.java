package com.bigfans.searchservice.schema.mapping;

import com.bigfans.framework.es.BaseMapping;
import com.bigfans.framework.es.schema.IndexSettingsAnalyzer_Pinyin;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

import java.io.IOException;

public class BrandMapping extends BaseMapping {

	public static final String INDEX = "brand";
	public static final String TYPE = "Brand";
	public static final String ALIAS = "brand_candidate";
	public static final String FIELD_ID = "id";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_NAME_RAW = "raw";
	public static final String FIELD_NAME_PINYIN = "pinyin";
	public static final String FIELD_CATEGORY_ID = "category";
	public static final String FIELD_CATEGORIES = "categories";
	public static final String FIELD_DESC = "description";
	public static final String FIELD_LOGO = "logo";
	public static final String FIELD_REC = "recommend";
	
	@Override
	public String getType() {
		return TYPE;
	}
	
	public String getIndex() {
		return INDEX;
	}
	
	public Object getMapping() {
		XContentBuilder schemaBuilder = null;
		try {
			schemaBuilder = XContentFactory.jsonBuilder()
					.startObject()
						.startObject(TYPE)
							.startObject("properties")
								.startObject(FIELD_ID)
									.field("type", "keyword")
									.field("store", "yes")
								.endObject()
								.startObject(FIELD_NAME)
									.field("type", "text")
									.field("index", "analyzed")
								    .field("analyzer", IndexSettingsAnalyzer_Pinyin.NAME)
									.startObject("fields")
					            		.startObject(FIELD_NAME_RAW)
					            			.field("type", "keyword")
					            		.endObject()
						            .endObject()
								.endObject()
								.startObject(FIELD_LOGO)  
						            .field("type", "keyword")  
						        .endObject()
						        .startObject(FIELD_DESC)  
						            .field("type", "text")  
						        .endObject()
						        .startObject(FIELD_REC)  
						            .field("type", "integer")  
						        .endObject()
						        .startObject(FIELD_CATEGORIES)  
						            .field("type", "keyword")  
						        .endObject()
						        .startObject(FIELD_CATEGORY_ID)  
						            .field("type", "keyword")  
						        .endObject()
						     .endObject()
						 .endObject()
					.endObject();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return schemaBuilder;
	}

}
