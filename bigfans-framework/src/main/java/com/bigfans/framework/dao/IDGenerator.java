package com.bigfans.framework.dao;

public interface IDGenerator {

	Object generate();
	
}
