package com.bigfans.paymentservice.model;

import com.bigfans.paymentservice.model.entity.PaymentEntity;
import lombok.Data;

import java.util.List;

/**
 * 
 * @Description:
 * @author lichong
 * 2015年4月5日下午6:36:32
 *
 */
@Data
public class Payment extends PaymentEntity {

}
