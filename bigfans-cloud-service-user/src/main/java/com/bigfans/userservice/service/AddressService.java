package com.bigfans.userservice.service;

import com.bigfans.framework.dao.BaseService;
import com.bigfans.userservice.model.Address;

import java.util.List;


/**
 * @author lichong
 * 2014年12月16日下午11:25:59
 * @Description:地址服务接口
 */
public interface AddressService extends BaseService<Address> {

    List<Address> listByUser(String userId) throws Exception;

    Address getUserAddress(String addressId, String userId) throws Exception;

    Address getUserDefaultAddress(String userId) throws Exception;

    Address getSysDefaultAddress() throws Exception;

    int delete(String uid, String addrId) throws Exception;

}
