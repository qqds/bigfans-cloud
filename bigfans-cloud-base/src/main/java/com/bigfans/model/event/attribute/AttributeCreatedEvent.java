package com.bigfans.model.event.attribute;

import com.bigfans.framework.event.AbstractEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author lichong
 * @create 2018-02-15 上午8:58
 **/
@Data
@NoArgsConstructor
public class AttributeCreatedEvent extends AbstractEvent {

    private String attributeId;

    public AttributeCreatedEvent(String attributeId) {
        this.attributeId = attributeId;
    }
}
