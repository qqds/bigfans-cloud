package com.bigfans.catalogservice.service.sku;

import com.bigfans.catalogservice.dao.StockLogDAO;
import com.bigfans.catalogservice.model.StockLog;
import com.bigfans.framework.dao.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service(StockLogServiceImpl.BEAN_NAME)
public class StockLogServiceImpl extends BaseServiceImpl<StockLog> implements StockLogService  {

    public static final String BEAN_NAME = "stockLogService";

    private StockLogDAO stockLogDAO;

    @Autowired
    public StockLogServiceImpl(StockLogDAO stockLogDAO) {
        super(stockLogDAO);
        this.stockLogDAO = stockLogDAO;
    }

    @Override
    public List<StockLog> listByOrder(String orderId) throws Exception {
        return stockLogDAO.listByOrder(orderId);
    }

    @Override
    public List<StockLog> listOrderStockOutLog(String orderId) throws Exception {
        return stockLogDAO.listByOrder(orderId , StockLog.DIR_OUT);
    }

    @Override
    public Integer countByOrder(String orderId) throws Exception {
        return stockLogDAO.countByOrder(orderId);
    }
}
