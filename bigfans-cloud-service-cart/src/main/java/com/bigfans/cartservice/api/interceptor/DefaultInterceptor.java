package com.bigfans.cartservice.api.interceptor;

import com.bigfans.framework.model.PageContext;
import com.bigfans.framework.web.CookieHolder;
import com.bigfans.framework.web.RequestHolder;
import com.bigfans.framework.web.ResponseHolder;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 默认拦截器,初始化参数
 * @author lichong
 *
 */
public class DefaultInterceptor implements HandlerInterceptor {
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		RequestHolder.setHttpServletRequest(request);
		ResponseHolder.setHttpServletResponse(response);
		return true;
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}

	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		RequestHolder.clear();
		ResponseHolder.clear();
		PageContext.clear();
	}

}
