package com.bigfans.catalogservice.model;

import com.bigfans.catalogservice.model.entity.CategoryAttributeEntity;
import lombok.Data;

/**
 * @author lichong
 * @create 2018-02-24 下午5:49
 **/
@Data
public class CategoryAttribute extends CategoryAttributeEntity {
}
